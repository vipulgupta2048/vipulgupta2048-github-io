run = [
  '<span class="indice">Run </span><span class="colored">portfolio(vipulgupta2048):</span>',
  'Let\'s get started <span class="colored">portfolio<span>',
  "To start enter the command <span class=\"command\">'help'<span>"
];

//password
var password = "Linux"; // NO MAJUSCULE
// Text for the command 'Help'
help = [
  "<br>",
  '<span class="command">about</span>          Learn more about me',
  '<span class="command">timeline</span>       My Developer Story',
  '<span class="command">social</span>         Displays social networks',
  '<span class="command">contact</span>        Get my email',
  '<span class="command">credits</span>        Show the credits',
  '<span class="command">download</span>       Support the designer',
  "<br>"
];
var download =
  "Thanks for your interest in getting the terminal portfolio. You can reach the designer here! --> valentin.salaud@mmibordeaux.com";
var email =
  '<a href="mailto:vipulgupta2048@gmail.com">vipulgupta2048[@]gmail[dot]com</a>';

// Text for the command 'about'
about = [
  "<br>",
  "Hi there!, I am a student, OSS developer and Devrel.",
  "I headhunt for StackRaft, write docs for Mixster, work for no-one (Unemployed freelancer)",
  "Constantly search for new places to eat",
  "I am into Product hacking, growth as well as closing issues on GitHub",
  "I dig chicks, chinese food and indie music",
  'If you like to know more about me, <a href="https://mixstersite.wordpress.com/aboutvipulgupta2048">Mixster can help you with that &#128076;</a>',
  "Available all over the web as vipulgupta2048",
  "<br>"
];

//link social
var github = "https://github.com/vipulgupta2048/";
var linkedin = "https://www.linkedin.com/in/vipulgupta2048/";
var twitter = "https://twitter.com/vipulgupta2048/";
var instagram = "https://www.instagram.com/vipulgupta2048_/";
var blog = "https://mixstersite.wordpress.com";

//social
social = [
  "<br>",
  'github         <a href="' + github + '">' + github + "</a>",
  'linkedin       <a href="' + linkedin + '">' + linkedin + "</a>",
  'twitter        <a href="' + twitter + '">' + twitter + "</a>",
  'instagram      <a href="' + instagram + '">' + instagram + "</a>",
  'Mixster        <a href="' + blog + '">' + blog + "</a>",
  "<br>"
];

//open windows
var openWindow = "new open window";

//works
var worksOpen = "works open";
var worksClos = "works close";

//credits
credits = [
  "<br>",
  '<span class="margin">All credits to Valentin SLD for desiging this portfolio.</span>',
  '<span class="margin">If you loved his work, do support or better yet hire him.</span>',
  '<span class="margin">His behance profile - https://www.behance.net/valentinsld</span>',
  '<span class="margin">Bad developers steal. Good Developers share.</span>',
  '<span class="margin">&#128151; to all open-source softwares, frameworks, libraries, scripts used.</span>',
  "<span>&#128035;</span>",
  "<br>"
];
